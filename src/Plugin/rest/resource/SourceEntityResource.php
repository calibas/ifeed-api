<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get Feed Source nodes
 *
 * @RestResource(
 *   id = "source_entity_resource",
 *   label = @Translation("Source Entity Resource"),
 *   entity_type = "node",
 *   serialization_class = "Drupal\node\Entity\Node",
 *   uri_paths = {
 *     "canonical" = "/api/source/{node}"
 *   }
 * )
 */
class SourceEntityResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        if($entity->bundle() !== 'feed_source') {
            return (new ResourceResponse(['message'=>'Invalid ID.']));
        }
        if(!$entity->access('view')){
            return (new ResourceResponse(['message'=>'Unpublished post.']));
        }
        $request = \Drupal::request();
        $offset = $request->query->get('offset', 0);
        if (!$entity->get('field_image')->isEmpty()) {
            $imageURL = file_create_url($entity->get('field_image')->entity->uri->value);
        } else {
            $imageURL = '';
        }

        if (!$entity->get('field_feed_source_url')->isEmpty()) {
            $sourceURL = $entity->get('field_feed_source_url')[0]->uri;
        } else {
            $sourceURL = '';
        }

        if (!$entity->get('field_importer')->isEmpty()) {
            $importer = $entity->get('field_importer')[0]->value;
        } else {
            $importer = 'rss_importer';
        }

        if (!$entity->get('field_page_parser')->isEmpty()) {
            $pageParser = $entity->get('field_page_parser')[0]->value;
        } else {
            $pageParser = 'default';
        }

        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', ['external_link', 'ifeed_video'], 'IN')
            ->condition('field_source', $entity->id())
            ->sort('created', 'DESC')
            ->range($offset, 10)
            ->accessCheck(false);
        $nids = $query->execute();
        $service = \Drupal::service('ifeedapi.apiservice');
        $nodes = $service->getPostInfo($nids);

        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            'image' => $imageURL,
            'created' => $entity->get('created')->value,
            'sourceURL' => $sourceURL,
            'importer' => $importer,
            'pageParser' => $pageParser,
            'posts' => $nodes
        ];

//        $build = array(
//            '#cache' => array(
//                'max-age' => 0,
//            ),
//        );
//        return (new ResourceResponse($response))->addCacheableDependency($build);
        $tag = 'ifeed_sourceentity_' . $entity->id();
        $build = array(
            '#cache' => [
                //'max-age' => 0,
                'contexts' => ['url.path', 'url.query_args'],
                'tags' => [$tag]
            ],
        );
        return (new ResourceResponse($response))->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
    }
}