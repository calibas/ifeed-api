<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "site_stats_resource",
 *   label = @Translation("Site Stats Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/stats",
 *   }
 * )
 */
class SiteStatsResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        $query = \Drupal::entityQuery('node')
            ->accessCheck(false)
             ->condition('status', 1);
        $nodeCount = $query->count()->execute();

        $query = \Drupal::entityQuery('user')
            ->accessCheck(false)
            ->condition('status', 1);
        $userCount = $query->count()->execute();

        $query = \Drupal::entityQuery('node')
            ->accessCheck(false)
            ->condition('type', 'feed_source')
            ->condition('status', 1)
            ->sort('created', 'DESC')
            ->range(0, 10);

        $latestFeedSources = $query->execute();
        $service = \Drupal::service('ifeedapi.apiservice');
        $feedSourceInfo = $service->getFeedSourceInfo($latestFeedSources);

        $response = [
            'nodeCount' => $nodeCount,
            'userCount' => $userCount,
            'latestFeeds' => $feedSourceInfo
        ];

        $build = array(
            '#cache' => array(
                'max-age' => 300,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);


        //return new ResourceResponse($response);
    }

}