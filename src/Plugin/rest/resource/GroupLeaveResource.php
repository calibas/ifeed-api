<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;

/**
 * Provides a resource
 *
 * @RestResource(
 *   id = "group_leave_resource",
 *   label = @Translation("Group Leave Resource"),
 *   entity_type = "group",
 *   serialization_class = "Drupal\group\Entity\Group",
 *   uri_paths = {
 *     "canonical" = "/api/group/{group}/leave",
 *   }
 * )
 */
class GroupLeaveResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        $uid = \Drupal::currentUser()->id();
        $user = \Drupal\user\Entity\User::load($uid);

        if($entity->hasPermission('leave group', $user)) {
            $entity->removeMember($user);
        }

        $groupMembers = $entity->getMembers();
        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            //'userid' => $this->currentUser->id(),
            'description' => $entity->get('field_description')->getValue(),
            'image' => $entity->get('field_image')->getValue(),
            'website' => $entity->get('field_website')->getValue(),
            'userCount' => count($groupMembers)
        ];
        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($entity);
    }

}