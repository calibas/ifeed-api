<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "group_list_resource",
 *   label = @Translation("Group List Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/groups",
 *   }
 * )
 */
class GroupListResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        $response = [];
        //$query = \Drupal::service('entity.query');
        $query = \Drupal::entityQuery('group');
        $group_ids = $query->execute();
        $groups = Group::loadMultiple($group_ids);
        foreach ($groups as $group) {
            //\Drupal::logger('demo_resource2')->notice();
            $groupMembers = $group->getMembers();
            $groupData = [
                'id' => $group->id(),
                'name' => $group->label(),
                'description' => $group->get('field_description')->getValue(),
                'image' => $group->get('field_image')->getValue(),
                'website' => $group->get('field_website')->getValue(),
                'userCount' => count($groupMembers)
            ];
            $response[] = $groupData;

        }
        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
    }

}