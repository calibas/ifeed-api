<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get post nodes
 *
 * @RestResource(
 *   id = "news_entity_resource",
 *   label = @Translation("News Entity Resource"),
 *   entity_type = "node",
 *   serialization_class = "Drupal\node\Entity\Node",
 *   uri_paths = {
 *     "canonical" = "/api/news/{node}",
 *     "https://www.drupal.org/link-relations/create" = "/api/create/news"
 *   }
 * )
 */
class NewsEntityResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        $postTypes = [
            'ifeed_video',
            'external_link'
        ];
        if(!in_array($entity->bundle(), $postTypes)) {
            return (new ResourceResponse(['message'=>'Invalid ID.']));
        }
        if(!$entity->access('view')){
            return (new ResourceResponse(['message'=>'Unpublished post.']));
        }
        if (!$entity->get('field_image')->isEmpty()) {
            $imageURL = file_create_url($entity->get('field_image')->entity->uri->value);
        } else {
            $imageURL = '';
        }


        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            'body' => $entity->get('body')[0]->processed,
            'image' => $imageURL,
            'created' => $entity->get('created')->value,
            'type' => $entity->bundle()
        ];

        if ($entity->bundle() == 'ifeed_video') {
            $response['videoID'] = $entity->get('field_external_id')[0]->value;
            $response['videoHost'] = $entity->get('field_external_host')[0]->value;
        }

        if ($entity->bundle() == 'external_link') {
            if (!$entity->get('field_original_article')->isEmpty()) {
                $response['sourceURL'] = $entity->get('field_original_article')[0]->uri;
            }
        }

        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
    }

    /**
     * Responds to POST requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     * @return ResourceResponse
     */
    public function post(EntityInterface $entity = NULL)
    {

        parent::post($entity);
        return new ResourceResponse($entity);
    }

    /**
     * Responds to PATCH requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface $original_entity
     *   The original entity.
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The current entity.
     *
     * @return ResourceResponse
     */
    public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL)
    {

        parent::patch($original_entity, $entity);

        return new ResourceResponse($entity, 200);
    }
}