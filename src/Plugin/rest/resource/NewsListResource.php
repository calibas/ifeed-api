<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\CacheableMetadata;
//use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
//use Drupal\node\Entity\Node;
//use Drupal\image\Entity\ImageStyle;


/**
 * Provides a resource to get a list of latest posts
 *
 * @RestResource(
 *   id = "news_list_resource",
 *   label = @Translation("News List Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/news",
 *   }
 * )
 */
class NewsListResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        $request = \Drupal::request();
        $offset = $request->query->get('offset', 0);
        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', ['external_link', 'ifeed_video'], 'IN')
            ->sort('created', 'DESC')
            ->range($offset, 10)
            ->accessCheck(false);

        $news_ids = $query->execute();
        $service = \Drupal::service('ifeedapi.apiservice');
        $response = $service->getPostInfo($news_ids);
//        $news = Node::loadMultiple($news_ids);
//        foreach ($news as $item) {
//            //\Drupal::logger('demo_resource2')->notice();
//            // Set known values
//            $newsData = [
//                'id' => $item->id(),
//                'name' => $item->label(),
//                'body' => $item->get('body')[0]->processed,
//                'created' => $item->get('created')->value,
//                'type' => $item->bundle()
//            ];
//            // Create image URL
//            if (!$item->get('field_image')->isEmpty()) {
//                $newsData['image'] = file_create_url($item->get('field_image')->entity->uri->value);
//            } else {
//                $newsData['image'] = '';
//            }
//            if ($item->bundle() == 'external_link') {
//                if (!$item->get('field_original_article')->isEmpty()) {
//                    $newsData['sourceURL'] = $item->get('field_original_article')[0]->uri;
//                } else {
//                    $newsData['sourceURL'] = '';
//                }
//            }
//            if ($item->bundle() == 'ifeed_video') {
//                $newsData['videoID'] = $item->get('field_external_id')[0]->value;
//                $newsData['videoHost'] = $item->get('field_external_host')[0]->value;
//            }
//            if (!$item->get('field_source')->isEmpty()) {
//                $sourceEntity = $item->field_source->entity;
//                $source = [
//                    'id' => $sourceEntity->id(),
//                    'name' => $sourceEntity->label()
//                ];
//                if (!$sourceEntity->get('field_image')->isEmpty()) {
//                    $source['image'] = ImageStyle::load('medium')->buildUrl($sourceEntity->get('field_image')->entity->uri->value);
//                    //$source['image'] = file_create_url($sourceEntity->get('field_image')->entity->uri->value);
//                } else {
//                    $source['image'] = '';
//                }
//                $newsData['source'] = $source;
//            } else {
//                $newsData['source'] = '';
//            }
//            $response[] = $newsData;
//
//        }
        $tag = 'ifeed_newslist_' . $offset;
        $build = array(
            '#cache' => [
                //'max-age' => 0,
                'contexts' => ['url.path', 'url.query_args'],
                'tags' => [$tag]
            ],
        );
        return (new ResourceResponse($response))->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
    }

}