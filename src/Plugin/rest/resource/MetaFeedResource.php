<?php

namespace Drupal\ifeed_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;

/**
 * Provides a resource to get MetaFeed nodes
 *
 * @RestResource(
 *   id = "metafeed_resource",
 *   label = @Translation("MetaFeed Resource"),
 *   entity_type = "node",
 *   serialization_class = "Drupal\node\Entity\Node",
 *   uri_paths = {
 *     "canonical" = "/api/metafeed/{node}"
 *   }
 * )
 */
class MetaFeedResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        if($entity->bundle() !== 'metafeed') {
            return (new ResourceResponse(['message'=>'Invalid ID.']));
        }
        if(!$entity->access('view')){
            return (new ResourceResponse(['message'=>'Unpublished post.']));
        }
        //$request = \Drupal::request();
        //$offset = $request->query->get('offset', 0);
//        if (!$entity->get('field_image')->isEmpty()) {
//            $imageURL = file_create_url($entity->get('field_image')->entity->uri->value);
//        } else {
//            $imageURL = '';
//        }

        if (!$entity->get('body')->isEmpty()) {
            $body = $entity->get('body')[0]->value;
        } else {
            $body = '';
        }

        $feeds = [];
        if (!$entity->get('field_metafeed')->isEmpty()) {
            foreach($entity->get('field_metafeed') as $feed) {
                $feedNode = Node::load($feed->target_id);
                if (!$feedNode->get('field_image')->isEmpty()) {
                    $imageURL = file_create_url($feedNode->get('field_image')->entity->uri->value);
                } else {
                    $imageURL = '';
                }
                $feeds[] = [
                    'name' =>$feedNode->label(),
                   'id' => $feedNode->id(),
                    'image' => $imageURL
                ];
            }
        }

//        if (!$entity->get('field_page_parser')->isEmpty()) {
//            $pageParser = $entity->get('field_page_parser')[0]->value;
//        } else {
//            $pageParser = 'default';
//        }

//        $query = \Drupal::entityQuery('node')
//            ->condition('status', 1)
//            ->condition('type', ['external_link', 'ifeed_video'], 'IN')
//            ->condition('field_source', $entity->id())
//            ->sort('created', 'DESC')
//            ->range($offset, 10)
//            ->accessCheck(false);
//        $nids = $query->execute();
//        $service = \Drupal::service('ifeedapi.apiservice');
//        $nodes = $service->getPostInfo($nids);

        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            'created' => $entity->get('created')->value,
            'description' => $body,
            'feeds' => $feeds
            //'sourceURL' => $sourceURL,
            //'importer' => $importer,
            //'pageParser' => $pageParser,
            //'posts' => $nodes
        ];

//        $build = array(
//            '#cache' => array(
//                'max-age' => 0,
//            ),
//        );
//        return (new ResourceResponse($response))->addCacheableDependency($build);
        $tag = 'ifeed_metafeed_' . $entity->id();
        $build = array(
            '#cache' => [
                //'max-age' => 0,
                'contexts' => ['url.path', 'url.query_args'],
                'tags' => [$tag]
            ],
        );
        return (new ResourceResponse($response))->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
    }
}