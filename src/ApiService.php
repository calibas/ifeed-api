<?php

namespace Drupal\ifeed_api;

use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;

/**
 * iFeed's API Service.
 */
class ApiService
{
    /**
     * Creates an array of posts
     */
    public function getPostInfo($nids)
    {
        $postArray = [];
        $posts = Node::loadMultiple($nids);
        foreach ($posts as $node) {
            //\Drupal::logger('demo_resource2')->notice();
            // Set known values
            $postData = [
                'id' => $node->id(),
                'name' => $node->label(),
                'body' => $node->get('body')[0]->processed,
                'created' => $node->get('created')->value,
                'type' => $node->bundle()
            ];
            // Create image URL
            if (!$node->get('field_image')->isEmpty()) {
                $postData['image'] = file_create_url($node->get('field_image')->entity->uri->value);
            } else {
                $postData['image'] = '';
            }
            if ($node->bundle() == 'external_link') {
                if (!$node->get('field_original_article')->isEmpty()) {
                    $postData['sourceURL'] = $node->get('field_original_article')[0]->uri;
                } else {
                    $postData['sourceURL'] = '';
                }
            }
            if ($node->bundle() == 'ifeed_video') {
                $postData['videoID'] = $node->get('field_external_id')[0]->value;
                $postData['videoHost'] = $node->get('field_external_host')[0]->value;
            }
            if (!$node->get('field_source')->isEmpty()) {
                $sourceEntity = $node->field_source->entity;
                $source = [
                    'id' => $sourceEntity->id(),
                    'name' => $sourceEntity->label()
                ];
                if (!$sourceEntity->get('field_image')->isEmpty()) {
                    $source['image'] = ImageStyle::load('medium')->buildUrl($sourceEntity->get('field_image')->entity->uri->value);
                    //$source['image'] = file_create_url($sourceEntity->get('field_image')->entity->uri->value);
                } else {
                    $source['image'] = '';
                }
                $postData['source'] = $source;
            } else {
                $postData['source'] = '';
            }
            $postArray[] = $postData;

        }

        return $postArray;
    }

    /**
     * Creates an array of feed sources
     */
    public function getFeedSourceInfo($nids)
    {
        $postArray = [];
        $posts = Node::loadMultiple($nids);
        foreach ($posts as $node) {
                 $source = [
                    'id' => $node->id(),
                    'name' => $node->label()
                ];
                if (!$node->get('field_image')->isEmpty()) {
                    $source['image'] = ImageStyle::load('medium')->buildUrl($node->get('field_image')->entity->uri->value);
                    //$source['image'] = file_create_url($sourceEntity->get('field_image')->entity->uri->value);
                } else {
                    $source['image'] = '';
                }
                $postArray[] = $source;
        }

        return $postArray;
    }
}